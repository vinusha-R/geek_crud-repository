<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.geekmodel.Geek"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	align: center;
}

#firstDiv {
	background-color: lightgrey;
	width: 700px;
	border: 25px solid green;
	padding: 25px;
	margin: 25px;
}

#secondDiv {
	text-align: center;
}
</style>
</head>
<body>
	<%
		Geek geekDetails = (Geek) request.getAttribute("geekDetails");
		//out.println(geekDetails.getId());
		//out.println(geekDetails.getFirstName());
	%>
	<div id="firstDiv">
		<p style="text-align: center">Details</p>
		<div id="secondDiv">
			<table>
				<tr>
					<td>First Name :</td>
					<td>
						<%
							out.println(geekDetails.getFirstName());
						%>
					</td>
				</tr>
				<tr>
					<td>Last Name :</td>
					<td>
						<%
							out.println(geekDetails.getLastName());
						%>
					</td>
				</tr>
				<tr>
					<td>Mobile Number :</td>
					<td>
						<%
							out.println(geekDetails.getMobileNumber());
						%>
					</td>
				</tr>
				<tr>
					<td>Email id :</td>
					<td>
						<%
							out.println(geekDetails.getEmailId());
						%>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>