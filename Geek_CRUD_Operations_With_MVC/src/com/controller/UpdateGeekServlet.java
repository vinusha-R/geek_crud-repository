package com.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.geekdao.GeekDao;

/**
 * Servlet implementation class UpdateGeekServlet
 */
@WebServlet("/UpdateGeekServlet")
public class UpdateGeekServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateGeekServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//		String id = request.getParameter("updateId");
//		request.setAttribute("id", id);
//		RequestDispatcher requestDispatcher = request.getRequestDispatcher("UpdateGeekDetails.html");
//		requestDispatcher.forward(request, response);
//
//		//response.sendRedirect("UpdateGeekDetails.html");
//		String firstName = request.getParameter("firstName");
//		String lastName = request.getParameter("lastName");
//		String mobileNumber = request.getParameter("mobileNumber");
//		String gender = request.getParameter("gender");
//		String emailId = request.getParameter("emailId");
//		String password = request.getParameter("password");
//		GeekDao createRow = new GeekDao();
//		boolean status = createRow.update(id,firstName,lastName,mobileNumber,gender,emailId,password);
//		if(status == true) {
//			RequestDispatcher requestDispatcher2 = request.getRequestDispatcher("GeekUpdateStatus.jsp");
//			//response.sendRedirect("GeekUpdateStatus.jsp");
//			requestDispatcher.forward(request, response);
//		}
//		else {
//			RequestDispatcher requestDispatcher2 = request.getRequestDispatcher("UpdateGeek.html");
//			response.getWriter().println("enter valid id");
//			requestDispatcher.include(request, response);
//		}

		String id = request.getParameter("updateId");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String mobileNumber = request.getParameter("mobileNumber");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("pwd");
		GeekDao createRow = new GeekDao();
		boolean status = createRow.update(id, firstName, lastName, mobileNumber, gender, emailId, password);
		if (status == true) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GeekUpdateStatus.jsp");
			// response.sendRedirect("GeekUpdateStatus.jsp");
			requestDispatcher.forward(request, response);
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("UpdateGeek.html");
			response.getWriter().println("invalid id. Enter valid id");
			requestDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
