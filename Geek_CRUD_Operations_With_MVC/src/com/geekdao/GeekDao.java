package com.geekdao;

import java.sql.*;

import com.geekmodel.Geek;

public class GeekDao {
	public static Connection connectDatabase() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/GeekRepository", "root", "Vinusha@123");
		} catch (Exception e) {
			System.out.println(e);
		}
		return con;
	}

	public int generateId() {
		try {
			Statement statement = connectDatabase().createStatement();
			ResultSet resultSet = statement.executeQuery("select * from geek");
			resultSet.last();
			// System.out.println(rs.last());
			String lastId = resultSet.getString(1);
			// System.out.println(lastId);
			int id = Integer.parseInt(lastId.substring(1));
			return ++id;

		} catch (Exception e) {
			return 1;
		}
	}

	public Geek create(String firstName, String lastName, String mobileNumber, String gender, String emailId,
			String password) {
		int id = generateId();
		String newId = "G" + id;
		// can I directly set this id value or do I need to fetch from database and set
		// to setId() method???
		Geek geek = new Geek();
		geek.setId(newId);
		try {
			PreparedStatement preparedStatement = connectDatabase().prepareStatement(
					"insert into geek(id,firstName,lastName,gender,emailId,password,mobileNumber)values(?,?,?,?,?,?,?)");
			preparedStatement.setString(1, newId);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, gender);
			preparedStatement.setString(5, emailId);
			preparedStatement.setString(6, password);
			preparedStatement.setString(7, mobileNumber);
			int a = preparedStatement.executeUpdate();
			if (a != 0) {
				System.out.println("successfully added");
				return geek;
			} else {
				System.out.println("failed");
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public Geek read(String id) {
		try {
			Geek geek = new Geek();
			PreparedStatement preparedStatement = connectDatabase().prepareStatement("select * from geek where id =?");
			preparedStatement.setString(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				resultSet.beforeFirst();
				while (resultSet.next()) {
					geek.setId(resultSet.getString(1));
					geek.setFirstName(resultSet.getString(2));
					geek.setLastName(resultSet.getString(3));
					geek.setGender(resultSet.getString(4));
					geek.setEmailId(resultSet.getString(5));
					geek.setPassword(resultSet.getString(6));
					geek.setMobileNumber(resultSet.getString(7));
				}
				return geek;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public boolean update(String id, String firstName, String lastName, String mobileNumber, String gender,
			String emailId, String password) {
		try {
			PreparedStatement preparedStatement = connectDatabase().prepareStatement(
					"update geek set firstName = ?,lastName = ?,gender = ?,emailId =?,password = ?,mobileNumber = ? where id = ?");
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, gender);
			preparedStatement.setString(4, emailId);
			preparedStatement.setString(5, password);
			preparedStatement.setString(6, mobileNumber);
			preparedStatement.setString(7, id);
			int status = preparedStatement.executeUpdate();
			if (status != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean delete(String id) {
		try {
			PreparedStatement preparedStatement = connectDatabase().prepareStatement("delete from geek where id =?");
			preparedStatement.setString(1, id);
			int status = preparedStatement.executeUpdate();
			if (status != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
